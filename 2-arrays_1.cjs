const movies = [
    {
      title: "The Godfather",
      director: "Francis Ford Coppola",
      year: 1972,
      cast: ["Marlon Brando", "Al Pacino", "James Caan"],
      genre: ["Crime", "Drama"],
      rating: 9.2,
      duration: 175,
    },
    {
      title: "The Shawshank Redemption",
      director: "Frank Darabont",
      year: 1994,
      cast: ["Tim Robbins", "Morgan Freeman", "Bob Gunton"],
      genre: ["Drama"],
      rating: 9.3,
      duration: 142,
    },
    {
      title: "The Dark Knight",
      director: "Christopher Nolan",
      year: 2008,
      cast: ["Christian Bale", "Heath Ledger", "Aaron Eckhart"],
      genre: ["Action", "Crime", "Drama"],
      rating: 9.0,
      duration: 152,
    },
    {
      title: "Inception",
      director: "Christopher Nolan",
      year: 2010,
      cast: ["Leonardo DiCaprio", "Joseph Gordon-Levitt", "Ellen Page"],
      genre: ["Action", "Adventure", "Sci-Fi"],
      rating: 8.8,
      duration: 148,
    },
    {
      title: "The Matrix",
      director: "Lana Wachowski, Lilly Wachowski",
      year: 1999,
      cast: ["Keanu Reeves", "Laurence Fishburne", "Carrie-Anne Moss"],
      genre: ["Action", "Sci-Fi"],
      rating: 8.7,
      duration: 136,
    },
    {
      title: "Fight Club",
      director: "David Fincher",
      year: 1999,
      cast: ["Brad Pitt", "Edward Norton", "Helena Bonham Carter"],
      genre: ["Drama"],
      rating: 8.8,
      duration: 139,
    },
    {
      title: "Forrest Gump",
      director: "Robert Zemeckis",
      year: 1994,
      cast: ["Tom Hanks", "Robin Wright", "Gary Sinise"],
      genre: ["Drama", "Romance"],
      rating: 8.8,
      duration: 142,
    },
    {
      title: "Pulp Fiction",
      director: "Quentin Tarantino",
      year: 1994,
      cast: ["John Travolta", "Samuel L. Jackson", "Uma Thurman"],
      genre: ["Crime", "Drama"],
      rating: 8.9,
      duration: 154,
    },
    {
      title: "The Silence of the Lambs",
      director: "Jonathan Demme",
      year: 1991,
      cast: ["Jodie Foster", "Anthony Hopkins", "Lawrence A. Bonney"],
      genre: ["Crime", "Drama", "Thriller"],
      rating: 8.6,
      duration: 118,
    },
    {
      title: "The Usual Suspects",
      director: "Bryan Singer",
      year: 1995,
      cast: ["Kevin Spacey", "Gabriel Byrne", "Chazz Palminteri"],
      genre: ["Crime", "Mystery", "Thriller"],
      rating: 8.5,
      duration: 106,
    },
  ];
  
  
  
  // 1. Write a function that takes in a genre and returns an array of movie titles that belong to that genre, sorted in descending order of rating.

  function getMoviesByGenre(genre){
    let ans= movies.filter((movie)=>movie.genre.includes(genre))
    .sort((valueOne,valueTwo)=>valueTwo.rating-valueOne.rating)
    .map((movie)=>movie.title);
    console.log(ans);
  }
  
  //getMoviesByGenre("Action");
  
  // expected output: 
  // [
  //   "The Dark Knight",
  //   "Inception",
  //   "The Matrix",
  //   "The Lord of the Rings: The Return of the King",
  //   "The Avengers",
  //   "Terminator 2: Judgment Day"
  // ]
  
  
  
  
  // 2. Write a function that takes in an actor's name and returns an array of movie titles that the actor appeared in, sorted by year of release.
  
  function getMoviesByActor(actorName){
    let ans= movies.filter((movie)=>movie.cast.includes(actorName))
    .sort((valueOne,valueTwo)=>valueOne.year-valueTwo.year)
    .map((movie)=>movie.title);
    console.log(ans);
  }


  //getMoviesByActor("Leonardo DiCaprio");
  
  // expected output: 
  // [
  //   "Titanic",
  //   "The Departed",
  //   "The Wolf of Wall Street",
  //   "The Revenant"
  // ]
  
  
  // 3. Write a function that takes in a director's name and returns an object with the director's name as -
  // - a key and an array of movie titles that the director directed as the value.

  function getMoviesByDirector(director){
    let ans={};
    ans[director]=movies.filter((movie)=>movie.director==director)
    .map((movie)=>movie.title);
    console.log(ans);
  }
  
  //getMoviesByDirector("Christopher Nolan");
  
  // expected output: 
  // {
  //   "Christopher Nolan": [
  //     "The Dark Knight",
  //     "Inception",
  //     "Interstellar",
  //     "Dunkirk",
  //     "Memento"
  //   ]
  // }
  
  
  // 4. Write a function that takes in a year and returns an object with the year as a key and an array of movie 
  // titles released in that year as the value, sorted in descending order of rating.
  
  function getMoviesByYear(year){
    let ans={};
    ans[year]=movies.filter((movie)=>movie.year==year)
    .sort((movieOne,movieTwo)=>movieTwo.rating-movieOne.rating)
    .map((movie)=>movie.title);
    console.log(ans);
  }



  //getMoviesByYear(1994);
  
  // expected output: 
  // {
  //   1994: [
  //     "The Shawshank Redemption",
  //     "Pulp Fiction",
  //     "Forrest Gump"
  //   ]
  // }
  
  
  // 5. Write a function that returns an array of objects, where each object contains a unique combination of director and genre. 
  // Each object should have a "director" key and a "genre" key, and the values of these keys should be arrays of movie titles that
  //  match the combination of director and genre. The movie titles should be sorted in descending order of rating.
  
  function getMoviesByDirectorAndGenre(){
    
    let uniquePairs=movies.reduce((uniquePairs,movie)=>{
        movie.genre.forEach((genre)=>{
            uniquePairs[movie.director+"$"+genre]=1;
        })
        return uniquePairs;
    },{});

    //console.log(uniquePairs);
    let finalAns=[];
    for(let pairs in uniquePairs){
        let pair=pairs.split("$");
        let ans=movies.filter((movie)=>{
            if(movie.director==pair[0] && movie.genre.includes(pair[1])){
                return true;
            }
        }).map((movie)=>movie.title);
        //console.log(ans);
        finalAns.push({director:pair[0],genre:pair[1],movies:ans});
    }
    console.log(finalAns);
  }


   getMoviesByDirectorAndGenre();
  
  
  // expected output: 
  // [
  //   {
  //     director: "Christopher Nolan",
  //     genre: "Action",
  //     movies: ["The Dark Knight"]
  //   },
  //   {
  //     director: "Christopher Nolan",
  //     genre: "Thriller",
  //     movies: ["Inception"]
  //   },
  //   {
  //     director: "Christopher Nolan",
  //     genre: "Sci-Fi",
  //     movies: ["Interstellar"]
  //   },
  //   {
  //     director: "Steven Spielberg",
  //     genre: "Action",
  //     movies: ["Jurassic Park"]
  //   },
  //   {
  //     director: "Steven Spielberg",
  //     genre: "Drama",
  //     movies: ["Schindler's List"]
  //   },
  //   {
  //     director: "Steven Spielberg",
  //     genre: "Adventure",
  //     movies: ["Indiana Jones and the Raiders of the Lost Ark"]
  //   }
  // ]