function fetchUsers(){
    return new Promise((resolve,reject)=>{
        const response=fetch('https://jsonplaceholder.typicode.com/users');
        response.then((response1)=>{
            if(response1.status==200){
                resolve(response1.json());
            }
            else{
                reject("fail");
            }
        });
    })
}

function fetchTodos(){
    return new Promise((resolve,reject)=>{
        const response=fetch('https://jsonplaceholder.typicode.com/todos');
        response.then((response1)=>{
            if(response1.status==200){
                resolve(response1.json());
            }
            else{
                reject("fail");
            }
        })
    })
}


fetchTodos()
.then((response)=>console.log(response))
.catch((error)=>console.log(error));