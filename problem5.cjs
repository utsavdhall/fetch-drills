function fetchUsers(){
    return new Promise((resolve,reject)=>{
        const response=fetch('https://jsonplaceholder.typicode.com/users');
        response.then((response1)=>{
            if(response1.status==200){
                resolve(response1.json());
            }
            else{
                reject("fail");
            }
        });
    })
}

function fetchTodos(){
    return new Promise((resolve,reject)=>{
        const response=fetch('https://jsonplaceholder.typicode.com/todos');
        response.then((response1)=>{
            if(response1.status==200){
                resolve(response1.json());
            }
            else{
                reject("fail");
            }
        })
    })
}

let x=fetchUsers();
let y=fetchTodos();
Promise.all([x,y]).then((values) => {
    let todos=values[1][0];
    let users=values[0];
    let ans=users.filter((user)=>user.id==todos.userId);
    ans[0].title=todos.title;
    console.log(ans[0]);
  });
